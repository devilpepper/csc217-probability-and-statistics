N = 10000;     %Number of simulations
U = rand(3,N); %a 3-by-N matrix of random numbers from [0,1]
Y = (U < 0.5); %Y=1 (heads) if U < 0.5, otherwise Y=0 (tails)
X = sum(Y);    %Sums across columns. X = number of heads
hist(X);
