int bernoulliTrial(double p)
{
	//p = probability of success
	//return 1 or 0(true or false)
	//depending on uniform random number(0...1)
	return (rand() < p);
}

int binomial(int n, double p)
{
	int X = 0;
	//binomial variables are the sum of
	//bernoulli trials.(sum of successes)
	for(int i=0; i<n; i++) X+= bernoulliTrial(p);
	return X;
}

int geometric(double p)
{
	int X = 0;
	//Geometric variable is the
	//number of trials between successes
	do
	{
		X++;
	}while(!bernoulliTrial(p));
	return X;
}

int negativeBinomial(int k, double p)
{
	int X=0;
	//negative binomial variable are the sum of
	//k independent geometric variables
	for(int i=0; i<k; i++) X+=geometric(p);
	return X;
}

/******discrete random variables******/
//1. Divide [0,1] into subintervals:
	//A_0 = [0, p_0)
	//A_1 = [p_0, p_0+p_1)
	//A_2 = [p_0+p_1, p_0+p_1+p_2)
//2. Obtain a Standard Uniform random variable U
//3. If U is in A_i, let X = x_i

double discreteRandomX(vector<double>& X, vector<double>& P)
{
	//p_i = p(X=x_i) = P(U is in cdf_i)
	double U = rand();
	vector<double> cdf;
	cdf.push_back(0);
	int i;
	for(i=0; i<P.size(); i++)
		cdf.push_back(cdf.back() + P[i]);
	i=1; //skip nonsense 0
	while(U >= cdf[i] && i < cdf.size()) i++;

	return X[i];
}

int Poisson(double lambda)
{
	//p_i = P(X =x_i) = (e^-lambda)(lambda^i)/i!
	double U = rand(), F, F_0;
	int i= 0;
	F = F_0 = exp(-lambda);
	while(U >= F) F+= F_0 * pow(lambda, ++i) / fact(i);

	return i;
}

/******continuous random variables******/
//1. Obtain a Standard Uniform random variable U
//2. Compute X = inverseF(U) (solve F(X) = U for X)

double exponential(double lambda)
{
	//F(X) = 1-e^(-lambda*X) = U
	//X = -ln(U-1)/lambda
	//Since U and U-1 are both uniform, we can
	//use X = -ln(U)/lambda for the desired distribution
	//even though this is algebreically wrong
	double U = rand();
	return -ln(U)/lambda;
}

double gamma(int alpha, double lambda)
{
	double X = 0;
	for(int i=0; i<alpha; i++)
		X += exponential(lambda);
	return X;
}

/******discrete random variables using inverse method(sort of)******/
//1. Obtain a Standard Uniform random variable U
//2. Compute X = min{x in S | F(x) > U}

int geometricF(double p)
{
	//cdf = F(x) = 1 - (1-p)^x > U
	//solve for x
	//x > ln(1-U)/ln(1-p)
	double U = rand();
	return ceil(ln(1-U)/ln(1-p));
}
